<?php

namespace App\Form;

use App\Entity\Sport;
use App\Repository\SportRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdvancedSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('departement', ChoiceType::class, [
                'choices' => [
                    "01 - AIN" => "01 - AIN",
                    "02 - AISNE" => "02 - AISNE",
                    "03 - ALLIER" => "03 - ALLIER",
                    "04 - ALPES DE HAUTE PROVENCE" => "04 - ALPES DE HAUTE PROVENCE",
                    "05 - HAUTES-ALPES" => "05 - HAUTES-ALPES",
                    "06 - ALPES MARITIMES" => "06 - ALPES MARITIMES",
                    "07 - ARDECHE" => "07 - ARDECHE",
                    "08 - ARDENNES" => "08 - ARDENNES",
                    "09 - ARIEGE" => "09 - ARIEGE",
                    "10 - AUBE" => "10 - AUBE",
                    "11 - AUDE" => "11 - AUDE",
                    "12 - AVEYRON" => "12 - AVEYRON",
                    "13 - BOUCHES-DU-RHONE" => "13 - BOUCHES-DU-RHONE",
                    "14 - CALVADOS" => "14 - CALVADOS",
                    "15 - CANTAL" => "15 - CANTAL",
                    "16 - CHARENTE" => "16 - CHARENTE",
                    "17 - CHARENTE-MARITIME" => "17 - CHARENTE-MARITIME",
                    "18 - CHER" => "18 - CHER",
                    "19 - CORREZE" => "19 - CORREZE",
                    "21 - COTE-D'OR" => "21 - COTE-D'OR",
                    "22 - COTES D'ARMOR" => "22 - COTES D'ARMOR",
                    "23 - CREUSE" => "23 - CREUSE",
                    "24 - DORDOGNE" => "24 - DORDOGNE",
                    "25 - DOUBS" => "25 - DOUBS",
                    "26 - DRÔME" => "26 - DRÔME",
                    "27 - EURE" => "27 - EURE",
                    "28 - EURE-ET-LOIR" => "28 - EURE-ET-LOIR",
                    "29 - FINISTERE" => "29 - FINISTERE",
                    "2A - CORSE DU SUD" => "2A - CORSE DU SUD",
                    "2B - HAUTE CORSE" => "2B - HAUTE CORSE",
                    "30 - GARD" => "30 - GARD",
                    "31 - HAUTE-GARONNE" => "31 - HAUTE-GARONNE",
                    "32 - GERS" => "32 - GERS",
                    "33 - GIRONDE" => "33 - GIRONDE",
                    "34 - HERAULT" => "34 - HERAULT",
                    "35 - ILLE-ET-VILAINE" => "35 - ILLE-ET-VILAINE",
                    "36 - INDRE" => "36 - INDRE",
                    "37 - INDRE-ET-LOIRE" => "37 - INDRE-ET-LOIRE",
                    "38 - ISERE" => "38 - ISERE",
                    "39 - JURA" => "39 - JURA",
                    "40 - LANDES" => "40 - LANDES",
                    "41 - LOIR-ET-CHER" => "41 - LOIR-ET-CHER",
                    "42 - LOIRE" => "42 - LOIRE",
                    "43 - HAUTE-LOIRE" => "43 - HAUTE - LOIRE",
                    "44 - LOIRE-ATLANTIQUE" => "44 - LOIRE-ATLANTIQUE",
                    "45 - LOIRET" => "45 - LOIRET",
                    "46 - LOT" => "46 - LOT",
                    "47 - LOT-ET-GARONNE" => "47 - LOT-ET-GARONNE",
                    "48 - LOZERE" => "48 - LOZERE",
                    "49 - MAINE-ET-LOIRE" => "49 - MAINE-ET-LOIRE",
                    "50 - MANCHE" => "50 - MANCHE",
                    "51 - MARNE" => "51 - MARNE",
                    "52 - HAUTE - MARNE" => "52 - HAUTE-MARNE",
                    "53 - MAYENNE" => "53 - MAYENNE",
                    "54 - MEURTHE-ET-MOSELLE" => "54 - MEURTHE-ET-MOSELLE",
                    "55 - MEUSE" => "55 - MEUSE",
                    "56 - MORBIHAN" => "56 - MORBIHAN",
                    "57 - MOSELLE" => "57 - MOSELLE",
                    "58 - NIEVRE" => "58 - NIEVRE",
                    "59 - NORD" => "59 - NORD",
                    "60 - OISE" => "60 - OISE",
                    "61 - ORNE" => "61 - ORNE",
                    "62 - PAS-DE-CALAIS" => "62 - PAS-DE-CALAIS",
                    "63 - PUY-DE-DOME" => "63 - PUY-DE-DOME",
                    "64 - PYRENNES-ATLANTIQUES" => "64 - PYRENNES-ATLANTIQUES",
                    "65 - HAUTES-PYRENNES" => "65 - HAUTES-PYRENNES",
                    "66 - PYRENNES-ORIENTALES" => "66 - PYRENNES-ORIENTALES",
                    "67 - BAS-RHIN" => "67 - BAS-RHIN",
                    "68 - HAUT-RHIN" => "68 - HAUT-RHIN",
                    "69 - RHONE" => "69 - RHONE",
                    "70 - HAUTE-SAONE" => "70 - HAUTE-SAONE",
                    "71 - SAONE-ET-LOIRE" => "71 - SAONE-ET-LOIRE",
                    "72 - SARTHE" => "72 - SARTHE",
                    "73 - SAVOIE" => "73 - SAVOIE",
                    "74 - HAUTE-SAVOIE" => "74 - HAUTE-SAVOIE",
                    "75 - PARIS" => "75 - PARIS",
                    "76 - SEINE-MARITIME" => "76 - SEINE-MARITIME",
                    "77 - SEINE-ET-MARNE" => "77 - SEINE-ET-MARNE",
                    "78 - YVELINES" => "78 - YVELINES",
                    "79 - DEUX SEVRES" => "79 - DEUX SEVRES",
                    "80 - SOMME" => "80 - SOMME",
                    "81 - TARN" => "81 - TARN",
                    "82 - TARN-ET-GARONNE" => "82 - TARN-ET-GARONNE",
                    "83 - VAR" => "83 - VAR",
                    "84 - VAUCLUSE" => "84 - VAUCLUSE",
                    "85 - VENDEE" => "85 - VENDEE",
                    "86 - VIENNE" => "86 - VIENNE",
                    "87 - HAUTE-VIENNE" => "87 - HAUTE-VIENNE",
                    "88 - VOSGES" => "88 - VOSGES",
                    "89 - YONNE" => "89 - YONNE",
                    "90 - TERRITOIRE DE BELFORT" => "90 - TERRITOIRE DE BELFORT",
                    "91 - ESSONNE" => "91 - ESSONNE",
                    "92 - HAUTS-DE-SEINE" => "92 - HAUTS-DE-SEINE",
                    "93 - SEINE-SAINT-DENIS" => "93 - SEINE-SAINT-DENIS",
                    "94 - VAL-DE-MARNE" => "94 - VAL-DE-MARNE",
                    "95 - VAL-D'OISE" => "95 - VAL-D'OISE",
                    "971 - GUADELOUPE" => "971 - GUADELOUPE",
                    "972 - MARTINIQUE" => "972 - MARTINIQUE",
                    "973 - GUYANE" => "973 - GUYANE",
                    "974 - REUNION" => "974 - REUNION",
                    "975 - ST PIERRE ET MIQUELON" => "975 - ST PIERRE ET MIQUELON",
                    "984 - TERRES AUSTRALES ANTARCTIQUES" => "984 - TERRES AUSTRALES ANTARCTIQUES",
                    "985 - MAYOTTE" => "985 - MAYOTTE",
                    "986 - WALLIS ET FUTUNA" => "986 - WALLIS ET FUTUNA",
                    "987 - POLYNESIE FRANÇAISE" => "987 - POLYNESIE FRANÇAISE",
                    "988 - NOUVELLE-CALEDONIE" => "988 - NOUVELLE-CALEDONIE",
                ],
                'placeholder' => 'Sélectionner un département',
                'required' => true,
            ])
            ->add('design', EntityType::class, [
                'class' => Sport::class,
                'query_builder' => function (SportRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.design', 'ASC');
                },
                'choice_label' => 'design',
                'label' => 'Sport',
            ])
            ->add('niveau', ChoiceType::class, [
                'choices' => [
                    'Débutant' => 'Débutant',
                    'Confirmé' => 'Confirmé',
                    'Pro' => 'Pro',
                    'Supporter' => 'Supporter'
                ]
            ])
            ->add('rechercher', SubmitType::class);
    }
}
