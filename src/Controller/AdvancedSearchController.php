<?php

namespace App\Controller;

use App\Form\AdvancedSearchType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdvancedSearchController extends AbstractController
{
    /**
     * @Route("/advanced/search", name="advanced_search")
     */
    public function AdvancedSearch(Request $request, UserRepository $userRepository)
    {
        $users = [];
        $AdvancedSearchForm = $this->createForm(AdvancedSearchType::class);

        if ($AdvancedSearchForm->handleRequest($request)->isSubmitted() && $AdvancedSearchForm->isValid()) {
            $userData = $AdvancedSearchForm->getData();
            $users = $userRepository->FindByUser($userData);
        }
        return $this->render('advanced_search/index.html.twig', [
            'search_form' => $AdvancedSearchForm->createView(),
            'users' => $users
        ]);
    }
}
