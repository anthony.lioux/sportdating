<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Sport;
use App\Entity\Pratique;
use App\Form\RegistrationFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $sport = new Sport();
        $pratique = new Pratique();
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, [$user, $sport, $pratique]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setEmail($form->get('email')->getData());
            $user->setNom($form->get('nom')->getData());
            $user->setPrenom($form->get('prenom')->getData());
            $user->setDepartement($form->get('departement')->getData());
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $pratique->setNiveau($form->get('pratiques')->getData()->getNiveau());
            $sport->setDesign($form->get('sport')->getData()->getDesign());
            $sportID = $this->getDoctrine()->getRepository(Sport::class)->findOneBy(['design' => $form->get('sport')->getData()->getDesign()]);
            $pratique->setSport($sportID);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $pratique->setUser($user);
            $entityManager->persist($pratique);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('app_login');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
